import { Button, FlexBox , Text, Entry} from "luix";


export default class CustomComponent extends FlexBox {
    constructor(props: any = null){
        super(props);
    
        this.style = {height: "250px",  flexDirection: 'column',  justifyContent: 'space-between', alignItems: 'center', overflowWrap: 'break-word'};
        this.textEntry.EntryLabel.Text = "Enter Something:";
        this.textEntry.EntryInput.onChange = (e: Event)=>{
            e.stopPropagation();

            var txt = (e.target as HTMLInputElement).value;

            this.mirrorText.Text = txt;
            
            this.mirrorText.changed(this.props);
        }

        this.push(this.textEntry);


        this.mirrorText.style = {backgroundColor: '#fff', fontSize: '30px', overflow:'auto', maxWidth:'300px',  overflowWrap: 'break-word'}
        this.push(this.mirrorText);

        
    }
 
    private mirrorText = new Text();

    private textEntry = new Entry();
 
}