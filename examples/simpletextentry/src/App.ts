import { Elem, FlexBox} from 'luix';
 
import CustomComponent from './CustomComponent';
 
export default class App extends Elem {

    constructor(props: any = null){
        super("div", props);

       // TruCrypto.TestSign();
       // TruCrypto.TestRSACompare();

      // return;
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto"
        };
 
        const mainRouter = new FlexBox();
        mainRouter.style={flexDirection: 'column', alignItems: 'center', justifyContent:'center', flexGrow: 1, width: "100%"};

   
        mainRouter.push(this.CustomComponent);

        this.children.push(mainRouter);
 
        setInterval(()=>{
            this.changed(this.props); // force full app reload.
        },2000);

    }   


    private CustomComponent: CustomComponent = new  CustomComponent();
 

}
