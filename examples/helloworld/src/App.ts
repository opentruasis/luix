import  { Elem, FlexBox, Text} from 'luix';
 
  
export default class App extends Elem {

    constructor(props: any = null){
        super("div", props);

        
        this.style = {
            backgroundColor: "#efefef",
            height: "100%",
            display: 'flex',
            flexDirection: 'column',
            alignContent: "center",
            alignItems: 'center',
            justifyContent: "space-between",
            overflowY: "auto"
        };
 
        const mainRouter: FlexBox = new FlexBox();
        
        mainRouter.style={flexDirection: 'column', alignItems: 'center', justifyContent:'center', flexGrow: 1, width: "100%"};

        mainRouter.push(new Text("HELLO, WORLD!"));

        this.children.push(mainRouter);
    }   
 

}
