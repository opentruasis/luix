import { Button, FlexBox , Text} from "luix";


export default class CustomComponent extends FlexBox {
    constructor(props: any = null){
        super(props);
    
        this.style = {height: "250px", flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center'};

        this.counterText.style = {fontSize: '30px'}
        this.push(this.counterText);

        const btn = new Button();

        btn.style = {border: 0, width: "100px", height: "50px", backgroundColor: "green", color: '#fff'};

        btn.innerText = "Click to Count";

        btn.onClick = () =>{
             this.counter++;

             this.changed();
        }

        this.push(btn);
    }

    private counter : number = 0;

    private counterText:any = new Text();


    public changed(props: any = null){

        this.counterText.Text = this.counter.toString();

        super.changed(props);
    }


}