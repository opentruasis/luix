export default class AppState {
    private Refresh;
    Initialize(refresh: (props: any) => void): void;
    private state;
    getState(): any;
    getStateAt(key: string): any;
    postData(key: string, val: any, fullRefresh: boolean): any;
    post(data: {
        [key: string]: any;
    }, fullRefresh: boolean): any;
    removeData(key: string, fullRefresh: boolean): any;
}
