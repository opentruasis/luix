var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var AppState = /** @class */ (function () {
    function AppState() {
        this.state = {};
    }
    AppState.prototype.Initialize = function (refresh) {
        this.Refresh = refresh;
    };
    AppState.prototype.getState = function () {
        return this.state;
    };
    AppState.prototype.getStateAt = function (key) {
        return this.state[key];
    };
    AppState.prototype.postData = function (key, val, fullRefresh) {
        this.state[key] = val;
        if (this.Refresh && fullRefresh) {
            this.Refresh(this.state);
        }
        return this.state;
    };
    AppState.prototype.post = function (data, fullRefresh) {
        this.state = __assign(__assign({}, this.state), data);
        if (this.Refresh && fullRefresh) {
            this.Refresh(this.state);
        }
        return this.state;
    };
    AppState.prototype.removeData = function (key, fullRefresh) {
        delete this.state[key];
        if (this.Refresh && fullRefresh) {
            this.Refresh(this.state);
        }
        return this.state;
    };
    return AppState;
}());
export default AppState;
//# sourceMappingURL=AppState.js.map