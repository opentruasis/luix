var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import Elem from '../Elem';
var ArrowLeft = /** @class */ (function (_super) {
    __extends(ArrowLeft, _super);
    function ArrowLeft(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, "DIV", props) || this;
        var _a = props || {}, _b = _a.width, width = _b === void 0 ? 24 : _b, _c = _a.height, height = _c === void 0 ? 24 : _c, _d = _a.color, color = _d === void 0 ? "#333" : _d;
        _this.style = { width: width + 'px', height: height + 'px' };
        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("width", width);
        svg.setAttribute("height", height);
        svg.setAttribute("viewBox", "0 0 24 24");
        svg.setAttribute("fill", color);
        svg.innerHTML = "<path d=\"M0 0h24v24H0V0z\" fill=\"none\"/><path d=\"M15.41 16.59L10.83 12l4.58-4.59L14 6l-6 6 6 6 1.41-1.41z\"/>";
        _this.Elem.appendChild(svg);
        return _this;
    }
    return ArrowLeft;
}(Elem));
export { ArrowLeft };
export default ArrowLeft;
//# sourceMappingURL=ArrowLeft.js.map