var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import Elem from '../Elem';
var RefreshIcon = /** @class */ (function (_super) {
    __extends(RefreshIcon, _super);
    function RefreshIcon(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, "DIV", props) || this;
        var _a = props || {}, _b = _a.width, width = _b === void 0 ? 24 : _b, _c = _a.height, height = _c === void 0 ? 24 : _c, _d = _a.color, color = _d === void 0 ? "#333" : _d;
        _this.style = { width: width + 'px', height: height + 'px' };
        var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute("width", width);
        svg.setAttribute("height", height);
        svg.setAttribute("viewBox", "0 0 24 24");
        svg.setAttribute("fill", color);
        svg.innerHTML = "<path d=\"M0 0h24v24H0z\" fill=\"none\"/><path d=\"M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z\"/>";
        _this.Elem.appendChild(svg);
        return _this;
    }
    return RefreshIcon;
}(Elem));
export { RefreshIcon };
export default RefreshIcon;
//# sourceMappingURL=RefreshIcon.js.map