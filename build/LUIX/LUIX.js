import AppState from './AppState';
import Elem from './Elem';
import Router from './router/Router';
var LUIX = /** @class */ (function () {
    function LUIX() {
        this.FocusedElement = null;
        //////////////////////////////////////////////////////////////////////////
    }
    Object.defineProperty(LUIX, "VDOM", {
        get: function () {
            return LUIX._virtualDOM;
        },
        set: function (vdom) {
            LUIX._virtualDOM = vdom;
        },
        enumerable: false,
        configurable: true
    });
    LUIX.initialize = function () {
        LUIX.VDOM = LUIX.buildDOMFromWindow(window);
    };
    LUIX.buildDOMFromWindow = function (window) {
        return new Elem("document", window.document);
    };
    LUIX.start = function (startElem) {
        LUIX.initialize();
        LUIX._appState.Initialize(startElem.changed.bind(startElem));
        startElem.id = "literaluiapp";
        startElem.style = { "height": "100%" };
        var listOfNodes = LUIX.depthFirstSearch('literalui', LUIX.VDOM, []);
        for (var _i = 0, listOfNodes_1 = listOfNodes; _i < listOfNodes_1.length; _i++) {
            var nd = listOfNodes_1[_i];
            nd.style = { "height": "100%" };
            nd.children.push(startElem);
            var literaluiElement = document.getElementById('literalui');
            literaluiElement.style.height = "100%";
            literaluiElement.appendChild(nd.render());
        }
        LUIX.post({ "screenWidth": window.innerWidth, "screenHeight": window.innerHeight });
        window.addEventListener('resize', function (event) {
            console.log(event.target.innerWidth, event.target.innerHeight);
            LUIX.post({ "screenWidth": event.target.innerWidth, "screenHeight": event.target.innerHeight }, true);
        }, true);
    };
    LUIX.depthFirstSearch = function (searchId, node, findings) {
        if (node.id == searchId) {
            findings.push(node);
        }
        for (var _i = 0, _a = node.children; _i < _a.length; _i++) {
            var nd = _a[_i];
            this.depthFirstSearch(searchId, nd, findings);
        }
        return findings;
    };
    LUIX.findParent = function (searchId, node) {
        var result = null;
        for (var _i = 0, _a = node.children; _i < _a.length; _i++) {
            var nd = _a[_i];
            if (nd.id == searchId) {
                return node;
            }
        }
        for (var _b = 0, _c = node.children; _b < _c.length; _b++) {
            var nd = _c[_b];
            result = LUIX.findParent(searchId, nd);
            if (result) {
                return result;
            }
        }
        return result;
    };
    LUIX.findElem = function (searchId, node) {
        if (node == null) {
            node = LUIX._virtualDOM;
            if (!node)
                return;
        }
        if (node.id == searchId) {
            return node;
        }
        var result = null;
        for (var _i = 0, _a = node.children; _i < _a.length; _i++) {
            var nd = _a[_i];
            result = LUIX.findElem(searchId, nd);
            if (result) {
                return result;
            }
        }
        return result;
    };
    LUIX.UpdateElem = function (updateNode) {
        if (updateNode) {
            var renderedData = updateNode.render();
            updateNode.replaceWith(renderedData);
            //focus  the focused element..
            updateNode.renderDone();
        }
    };
    //////////////////////////////////////////////////////////////////////////
    /////Router Interfaces////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////
    LUIX.GoTo = function (path) {
        LUIX.Router.GoTo(path);
    };
    LUIX.AddRoute = function (route) {
        LUIX.Router.AddRoute(route);
    };
    LUIX.ProcessRoute = function (path) {
        return LUIX.Router.ProcessPath(path);
    };
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    /////APP STATE Interface//////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////
    LUIX.getState = function () {
        return this._appState.getState();
    };
    LUIX.getStateAt = function (key) {
        return this._appState.getStateAt(key);
    };
    LUIX.postData = function (key, val, fullRefresh) {
        if (fullRefresh === void 0) { fullRefresh = true; }
        return this._appState.postData(key, val, fullRefresh);
    };
    LUIX.post = function (data, fullRefresh) {
        if (fullRefresh === void 0) { fullRefresh = true; }
        return this._appState.post(data, fullRefresh);
    };
    LUIX.removeData = function (key, fullRefresh) {
        if (fullRefresh === void 0) { fullRefresh = true; }
        return this._appState.removeData(key, fullRefresh);
    };
    LUIX.Router = new Router();
    LUIX._virtualDOM = null;
    LUIX._appState = new AppState();
    return LUIX;
}());
export { LUIX };
export default LUIX;
//# sourceMappingURL=LUIX.js.map