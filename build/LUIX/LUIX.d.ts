import Elem from './Elem';
import RouteEndpoint from './router/RouteEndpoint';
export declare class LUIX {
    private static Router;
    private static _virtualDOM;
    static get VDOM(): Elem;
    static set VDOM(vdom: Elem);
    private static initialize;
    private static buildDOMFromWindow;
    private static _appState;
    static start(startElem: Elem): void;
    private static depthFirstSearch;
    private static findParent;
    private static findElem;
    static UpdateElem(updateNode: Elem): void;
    FocusedElement: Elem;
    static GoTo(path: string): void;
    static AddRoute(route: RouteEndpoint): void;
    static ProcessRoute(path: string): RouteEndpoint;
    static getState(): any;
    static getStateAt(key: string): any;
    static postData(key: string, val: any, fullRefresh?: boolean): any;
    static post(data: {
        [key: string]: any;
    }, fullRefresh?: boolean): any;
    static removeData(key: string, fullRefresh?: boolean): void;
}
export default LUIX;
