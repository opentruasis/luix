var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import LUIX from './LUIX';
import { v4 as uuidv4 } from 'uuid';
var Elem = /** @class */ (function () {
    function Elem(type, props) {
        this.isFocused = false;
        this.styles = {};
        this.isFlexDisplay = false;
        ///SubRouts:
        this.LastURL = "";
        this.Routes = {};
        if (type == null || typeof type !== 'string') {
            throw Error('The element type must be a string');
        }
        this.elem = document.createElement(type);
        var _a = props || {}, _b = _a.id, id = _b === void 0 ? null : _b, _c = _a.children, children = _c === void 0 ? [] : _c, _d = _a.style, style = _d === void 0 ? {} : _d;
        Object.assign(this.elem.style, style);
        this.props = props;
        this.id = id || uuidv4();
        // this.elem.id = this.id;
        this.cleanAndSet(type, children);
    }
    Object.defineProperty(Elem.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (iD) {
            this._id = iD;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "Elem", {
        get: function () {
            return this.elem;
        },
        enumerable: false,
        configurable: true
    });
    Elem.prototype.cleanAndSet = function (type, children) {
        if (children === void 0) { children = []; }
        var cleanedChildren = [];
        if (children.length > 0) {
            for (var _i = 0, children_1 = children; _i < children_1.length; _i++) {
                var k = children_1[_i];
                cleanedChildren.push(this.cleanChildren(k));
            }
        }
        this.type = type;
        this.children = cleanedChildren;
    };
    Elem.prototype.changed = function (props) {
        if (Object.keys(this.Routes).length > 0) {
            //Do Routing instead of updating..
            var currentRoute = LUIX.getStateAt("currentRoute");
            if (currentRoute) {
                var currentPath = currentRoute.path;
                if (currentPath != this.LastURL) {
                    if (currentRoute.id in this.Routes) {
                        this.clear();
                        this.children.push(currentRoute.comp);
                    }
                }
                this.LastURL = currentPath;
            }
        }
        LUIX.UpdateElem(this);
        this.children.forEach(function (ch) {
            if (ch) {
                ch.changed(props);
            }
        });
    };
    Elem.prototype.cleanChildren = function (element) {
        return new Elem(element.nodeName, element);
    };
    Elem.prototype.render = function () {
        var _a;
        //has A child with focused Element.
        (_a = this.elem).append.apply(_a, this.children.map(function (ch) { return ch.render(); }));
        return this.elem;
    };
    Elem.prototype.renderDone = function () {
        if (this.isFocused) {
            console.log("Render Done..");
            console.log(this.isFocused);
            this.elem.focus();
            //  (this.elem as HTMLInputElement).select();
            //   (this.elem as HTMLInputElement).setSelectionRange(-1,-1);
        }
    };
    Object.defineProperty(Elem.prototype, "onClick", {
        set: function (event) {
            this.elem.onclick = event;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "onChange", {
        set: function (event) {
            this.elem.addEventListener("input", event);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "innerText", {
        set: function (content) {
            this.elem.innerText = content;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "innerHtml", {
        set: function (content) {
            this.elem.innerHTML = content;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "disabled", {
        set: function (isDisabled) {
            this.elem.disabled = isDisabled;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "style", {
        get: function () {
            return this.styles;
        },
        set: function (styles) {
            this.styles = __assign(__assign({}, this.styles), styles);
            Object.assign(this.elem.style, styles);
        },
        enumerable: false,
        configurable: true
    });
    Elem.prototype.addClass = function (className) {
        this.elem.classList.add(className);
    };
    Elem.prototype.removeClass = function (className) {
        this.elem.classList.remove(className);
    };
    Elem.prototype.replaceWith = function () {
        var _a;
        var nodes = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            nodes[_i] = arguments[_i];
        }
        (_a = this.elem).replaceWith.apply(_a, nodes);
    };
    Elem.prototype.clear = function () {
        this.children.splice(0, this.children.length);
        this.elem.innerHTML = "";
        // LiteralUI.ClearChildren(this);
    };
    Object.defineProperty(Elem.prototype, "src", {
        get: function () {
            this.elem.src;
            return this.elem.src;
        },
        set: function (val) {
            this.elem.src = val;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "SelectionChanged", {
        set: function (event) {
            this.elem.onchange = event;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "SelectedIndex", {
        get: function () {
            return this.elem.selectedIndex;
        },
        set: function (val) {
            this.elem.selectedIndex = val;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "Selected", {
        get: function () {
            return this.elem.value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "inputType", {
        get: function () {
            return this.elem.type;
        },
        set: function (val) {
            this.elem.type = val;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "files", {
        get: function () {
            return this.elem.files;
        },
        enumerable: false,
        configurable: true
    });
    Elem.prototype.focus = function () {
        this.elem.focus();
    };
    Object.defineProperty(Elem.prototype, "value", {
        get: function () {
            return this.elem.value;
        },
        set: function (val) {
            this.elem.value = val;
        },
        enumerable: false,
        configurable: true
    });
    Elem.prototype.scrollEnd = function () {
        this.elem.scrollLeft += this.elem.scrollWidth;
    };
    Elem.prototype.scrollLeft = function (delta) {
        this.elem.scrollLeft += delta;
    };
    Object.defineProperty(Elem.prototype, "onWheel", {
        set: function (event) {
            this.elem.onwheel = event;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "onScroll", {
        set: function (event) {
            this.elem.onscroll = event;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Elem.prototype, "visible", {
        get: function () {
            return this.elem.style.display !== "none";
        },
        set: function (val) {
            if (val) {
                if (this.isFlexDisplay) {
                    this.elem.style.display = "flex";
                }
                else {
                    this.elem.style.display = "block";
                }
            }
            else {
                this.elem.style.display = "none";
            }
        },
        enumerable: false,
        configurable: true
    });
    Elem.prototype.AddRoute = function (path, comp) {
        var route = {};
        route.id = uuidv4();
        var pathParts = path.split("/");
        pathParts.shift();
        var params = {};
        for (var _i = 0, pathParts_1 = pathParts; _i < pathParts_1.length; _i++) {
            var part = pathParts_1[_i];
            if (part.indexOf("$", 0) == 0) {
                params[part.substring(1)] = null;
            }
        }
        route.path = path;
        route.comp = comp;
        route.pathParts = pathParts;
        route.params = params;
        if (Object.keys(params).length > 0) {
            route.hasParams = true;
        }
        else {
            route.hasParams = false;
        }
        //route["params"] = string;
        this.Routes[route.id] = route;
        LUIX.AddRoute(route);
    };
    return Elem;
}());
export { Elem };
export default Elem;
//# sourceMappingURL=Elem.js.map