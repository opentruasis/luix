import Elem from '../Elem';
export declare class Label extends Elem {
    constructor(txt?: string, props?: any);
    text: string;
    render(): HTMLElement;
    changed(props: any): void;
}
export default Label;
