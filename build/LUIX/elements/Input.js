var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Elem } from '../Elem';
var Input = /** @class */ (function (_super) {
    __extends(Input, _super);
    function Input(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, "INPUT", props) || this;
        _this.inputType = "text";
        _this.style = { backgroundColor: "#ccc", borderWidth: "0px", borderRadius: "5px", minHeight: "25px" };
        _this.Elem.onchange = function () {
            _this.isFocused = true;
        };
        _this.Elem.onfocus = function () {
            _this.isFocused = true;
        };
        _this.Elem.onblur = function (e) {
            if (e.sourceCapabilities) {
                _this.isFocused = false;
            }
        };
        return _this;
    }
    Object.defineProperty(Input.prototype, "InputType", {
        get: function () {
            return this.inputType;
        },
        set: function (val) {
            this.inputType = val;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Input.prototype, "Value", {
        get: function () {
            return this.value;
        },
        set: function (val) {
            this.value = val;
        },
        enumerable: false,
        configurable: true
    });
    return Input;
}(Elem));
export { Input };
export default Input;
//# sourceMappingURL=Input.js.map