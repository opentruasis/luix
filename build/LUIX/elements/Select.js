var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import Elem from '../Elem';
var Select = /** @class */ (function (_super) {
    __extends(Select, _super);
    function Select(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, "SELECT", props) || this;
        _this.mySelectionIndex = 0;
        _this._optionList = [];
        _this.SelectionChanged = function (event) {
            _this.mySelectionIndex = _this.SelectedIndex;
            _this.onSelectedChanged();
            _this.SelectedIndex = _this.mySelectionIndex;
        };
        return _this;
    }
    Select.prototype.onSelectedChanged = function () {
        alert("Override onSelectedChanged");
    };
    ;
    Object.defineProperty(Select.prototype, "OptionList", {
        get: function () {
            return this._optionList;
        },
        set: function (options) {
            this.ClearOptions();
            this._optionList = options;
            this.children = this._optionList;
        },
        enumerable: false,
        configurable: true
    });
    Select.prototype.renderDone = function () {
        var _this = this;
        setTimeout(function () {
            _this.SelectedIndex = _this.mySelectionIndex;
        }, 100);
    };
    Select.prototype.AddOption = function (option) {
        this._optionList.push(option);
    };
    Select.prototype.RemoveOptionAt = function (key) {
        this._optionList.splice(key, 1);
    };
    Select.prototype.ClearOptions = function () {
        this._optionList = [];
        this._optionList.splice(0, this._optionList.length);
        this.clear();
    };
    return Select;
}(Elem));
export { Select };
export default Select;
//# sourceMappingURL=Select.js.map