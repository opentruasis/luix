import { FlexBox } from './FlexBox';
export declare class Switch extends FlexBox {
    constructor(props?: any);
    private switchLabel;
    private switchInput;
    private switchSpan;
    set LabelText(str: string);
    set onSwitch(func: any);
    set Disabled(isDisabled: boolean);
    get isOn(): boolean;
    set isOn(val: boolean);
}
export default Switch;
