var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { FlexBox } from './FlexBox';
import Textarea from './Textarea';
import Text from './Text';
var LongEntry = /** @class */ (function (_super) {
    __extends(LongEntry, _super);
    function LongEntry(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, props) || this;
        _this.EntryLabel = new Text();
        _this.EntryText = new Textarea();
        _this.style = { alignItems: 'flex-start', flexDirection: 'column', width: '100%' };
        _this.EntryLabel.style = { marginRight: "5px" };
        _this.EntryText.style = { marginRight: "5px" };
        _this.push(_this.EntryLabel);
        _this.EntryText.style = { width: '90%', margin: '5px', alignSelf: 'center' };
        _this.push(_this.EntryText);
        return _this;
    }
    LongEntry.prototype.render = function () {
        return _super.prototype.render.call(this);
    };
    LongEntry.prototype.changed = function (props) {
        _super.prototype.changed.call(this, props);
    };
    return LongEntry;
}(FlexBox));
export { LongEntry };
export default LongEntry;
//# sourceMappingURL=LongEntry.js.map