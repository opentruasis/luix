import { Elem } from '../Elem';
export declare class Input extends Elem {
    constructor(props?: any);
    get InputType(): string;
    set InputType(val: string);
    get Value(): string;
    set Value(val: string);
}
export default Input;
