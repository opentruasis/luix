var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import Elem from '../Elem';
var ProgressBar = /** @class */ (function (_super) {
    __extends(ProgressBar, _super);
    function ProgressBar(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, "DIV", props) || this;
        _this.style = { width: '100%', height: "20px", backgroundColor: "#333", border: "1px" };
        return _this;
    }
    return ProgressBar;
}(Elem));
export { ProgressBar };
export default ProgressBar;
//# sourceMappingURL=ProgressBar.js.map