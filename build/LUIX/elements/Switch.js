var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { FlexBox } from './FlexBox';
import Input from './Input';
import Label from './Label';
import Span from './Span';
var Switch = /** @class */ (function (_super) {
    __extends(Switch, _super);
    function Switch(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, props) || this;
        _this.switchLabel = new Label();
        _this.switchInput = new Input();
        _this.switchSpan = new Span();
        _this.switchSpan.addClass("slider");
        _this.switchSpan.addClass("round");
        _this.switchLabel.addClass("switch");
        _this.switchInput.InputType = "checkbox";
        _this.switchLabel.children.push(_this.switchInput);
        _this.switchLabel.children.push(_this.switchSpan);
        _this.push(_this.switchLabel);
        var elem = document.getElementById("SwitchCss");
        if (!elem) {
            var styleElem = document.createElement('style');
            styleElem.id = "SwitchCss";
            styleElem.innerHTML = "\n        .switch {\n            position: relative;\n            display: inline-block;\n            width: 50px;\n            height: 25px;\n          }\n          \n          .switch input { \n            opacity: 0;\n            width: 0;\n            height: 0;\n          }\n          \n          .slider {\n            position: absolute;\n            cursor: pointer;\n            top: 0;\n            left: 0;\n            right: 0;\n            bottom: 0;\n            background-color: #ccc;\n            -webkit-transition: .4s;\n            transition: .4s;\n          }\n          \n          .slider:before {\n            position: absolute;\n            content: \"\";\n            height: 25px;\n            width: 25px;\n         \n            background-color: white;\n            -webkit-transition: .4s;\n            transition: .4s;\n          }\n          \n          input:checked + .slider {\n            background-color: #333;\n          }\n          \n          input:focus + .slider {\n            box-shadow: 0 0 1px #333;\n          }\n          \n          input:checked + .slider:before {\n            -webkit-transform: translateX(25px);\n            -ms-transform: translateX(25px);\n            transform: translateX(25px);\n          }\n          \n          /* Rounded sliders */\n          .slider.round {\n            border-radius: 25px;\n            margin-left: 5px;\n          }\n          \n          .slider.round:before {\n            border-radius: 50%;\n          }\n         ";
            document.getElementsByTagName("head")[0].appendChild(styleElem);
        }
        return _this;
    }
    Object.defineProperty(Switch.prototype, "LabelText", {
        set: function (str) {
            this.innerText = str;
            this.changed(this.props);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Switch.prototype, "onSwitch", {
        set: function (func) {
            this.switchInput.onChange = func;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Switch.prototype, "Disabled", {
        set: function (isDisabled) {
            this.switchInput.Elem.disabled = isDisabled;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Switch.prototype, "isOn", {
        get: function () {
            return this.switchInput.Elem.checked;
        },
        set: function (val) {
            this.switchInput.Elem.checked = val;
        },
        enumerable: false,
        configurable: true
    });
    return Switch;
}(FlexBox));
export { Switch };
export default Switch;
//# sourceMappingURL=Switch.js.map