import Elem from '../Elem';
export declare class ElemList extends Elem {
    constructor(props?: any);
    push(newElem: Elem): void;
}
export default ElemList;
