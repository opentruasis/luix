var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import Elem from '../Elem';
var Text = /** @class */ (function (_super) {
    __extends(Text, _super);
    function Text(txt, props) {
        if (txt === void 0) { txt = ""; }
        if (props === void 0) { props = null; }
        var _this = _super.call(this, "DIV", props) || this;
        _this.text = "";
        _this.text = txt;
        return _this;
    }
    Object.defineProperty(Text.prototype, "Text", {
        get: function () {
            return this.text;
        },
        set: function (innerText) {
            this.text = innerText;
            this.innerText = innerText;
        },
        enumerable: false,
        configurable: true
    });
    Text.prototype.render = function () {
        this.innerText = this.text;
        return _super.prototype.render.call(this);
    };
    Text.prototype.changed = function (props) {
        _super.prototype.changed.call(this, props);
    };
    return Text;
}(Elem));
export { Text };
export default Text;
//# sourceMappingURL=Text.js.map