import { FlexBox } from './FlexBox';
import Input from './Input';
import Text from './Text';
export declare class Entry extends FlexBox {
    constructor(props?: any);
    EntryLabel: Text;
    EntryInput: Input;
    render(): HTMLElement;
    changed(props: any): void;
}
export default Entry;
