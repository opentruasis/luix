import Elem from '../Elem';
import SelectOption from './SelectOption';
export declare class Select extends Elem {
    constructor(props?: any);
    onSelectedChanged(): void;
    mySelectionIndex: number;
    private _optionList;
    set OptionList(options: SelectOption[]);
    renderDone(): void;
    get OptionList(): SelectOption[];
    AddOption(option: SelectOption): void;
    RemoveOptionAt(key: number): void;
    ClearOptions(): void;
}
export default Select;
