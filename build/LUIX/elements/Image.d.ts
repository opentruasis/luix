import Elem from '../Elem';
export declare class Image extends Elem {
    constructor(props?: any);
    get imgSource(): string;
    set imgSource(value: string);
}
export default Image;
