import Elem from '../Elem';
export declare class Text extends Elem {
    constructor(txt?: string, props?: any);
    private text;
    get Text(): string;
    set Text(innerText: string);
    render(): HTMLElement;
    changed(props: any): void;
}
export default Text;
