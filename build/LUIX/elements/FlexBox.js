var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import ElemList from './ElemList';
var FlexBox = /** @class */ (function (_super) {
    __extends(FlexBox, _super);
    function FlexBox(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, props) || this;
        var _a = (props || {}).style, style = _a === void 0 ? {} : _a;
        _this.isFlexDisplay = true;
        _this.style = __assign({ display: 'flex', flexDirection: "row", justifyContent: 'space-around' }, style);
        return _this;
    }
    return FlexBox;
}(ElemList));
export { FlexBox };
export default FlexBox;
//# sourceMappingURL=FlexBox.js.map