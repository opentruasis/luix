import { FlexBox } from './FlexBox';
import Textarea from './Textarea';
import Text from './Text';
export declare class LongEntry extends FlexBox {
    constructor(props?: any);
    EntryLabel: Text;
    EntryText: Textarea;
    render(): HTMLElement;
    changed(props: any): void;
}
export default LongEntry;
