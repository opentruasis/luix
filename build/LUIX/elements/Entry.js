var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { FlexBox } from './FlexBox';
import Input from './Input';
import Text from './Text';
var Entry = /** @class */ (function (_super) {
    __extends(Entry, _super);
    function Entry(props) {
        if (props === void 0) { props = null; }
        var _this = _super.call(this, props) || this;
        _this.EntryLabel = new Text();
        _this.EntryInput = new Input();
        _this.style = { alignItems: 'center' };
        _this.EntryLabel.style = { marginRight: "5px" };
        _this.EntryInput.style = { marginRight: "5px" };
        _this.push(_this.EntryLabel);
        _this.push(_this.EntryInput);
        return _this;
    }
    Entry.prototype.render = function () {
        return _super.prototype.render.call(this);
    };
    Entry.prototype.changed = function (props) {
        _super.prototype.changed.call(this, props);
    };
    return Entry;
}(FlexBox));
export { Entry };
export default Entry;
//# sourceMappingURL=Entry.js.map