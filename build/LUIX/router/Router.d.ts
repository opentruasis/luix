import RouteEndpoint from './RouteEndpoint';
export default class Router {
    constructor();
    GoTo(path: string): void;
    private ProcessSearch;
    ProcessPath(path: string): RouteEndpoint;
    private Routes;
    AddRoute(route: RouteEndpoint): void;
}
