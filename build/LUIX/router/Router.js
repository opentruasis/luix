var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import LUIX from "../LUIX";
var Router = /** @class */ (function () {
    function Router() {
        var _this = this;
        this.Routes = {};
        window.addEventListener('popstate', function (event) {
            var currentPath = window.history.state ? window.history.state : "/";
            var route = _this.ProcessPath(currentPath);
            var searchObj = _this.ProcessSearch(window.location.search);
            if (route) {
                route.search = searchObj;
                LUIX.postData("currentRoute", route);
            }
        }, false);
        setTimeout(function () {
            var currentPath = window.location.pathname ? window.location.pathname : "/";
            var route = _this.ProcessPath(currentPath);
            var searchObj = _this.ProcessSearch(window.location.search);
            if (route) {
                route.search = searchObj;
                LUIX.postData("currentRoute", route);
            }
        }, 10);
    }
    Router.prototype.GoTo = function (path) {
        window.history.pushState(path, path, path);
        var route = this.ProcessPath(path);
        var searchObj = this.ProcessSearch(path);
        route.search = searchObj;
        LUIX.postData("currentRoute", route);
    };
    Router.prototype.ProcessSearch = function (searchStr) {
        try {
            var searchSplit = searchStr.split("?");
            searchSplit.shift();
            var searchKeyVal = {};
            if (searchSplit.length > 0) {
                var multiQuery = searchSplit[0].split("&");
                for (var _i = 0, multiQuery_1 = multiQuery; _i < multiQuery_1.length; _i++) {
                    var dataStr = multiQuery_1[_i];
                    var splitKeyVal = dataStr.split("=");
                    searchKeyVal[splitKeyVal[0]] = splitKeyVal[1];
                }
            }
            return searchKeyVal;
        }
        catch (e) {
            return {};
        }
    };
    Router.prototype.ProcessPath = function (path) {
        console.log("Process Path...");
        console.log(path);
        var endpoint = null;
        var currentPathParts = path.split("?");
        console.log("mmmmm");
        console.log(currentPathParts);
        if (currentPathParts.length == 2) {
            currentPathParts = [currentPathParts[0]];
        }
        currentPathParts = currentPathParts[0].split("/");
        currentPathParts.shift(); // get rid of first elem.
        console.log(currentPathParts);
        var currentParams = {};
        for (var routeId in this.Routes) {
            var routeEndpoint = this.Routes[routeId];
            console.log("......");
            console.log(routeEndpoint);
            var partcount = 0;
            var isAMatch = true;
            for (var _i = 0, _a = routeEndpoint.pathParts; _i < _a.length; _i++) {
                var part = _a[_i];
                if (part.indexOf("$", 0) == 0) {
                    //is a variable position..
                    //then param..
                    var paramKey = part.substring(1);
                    if (paramKey in routeEndpoint.params) {
                        //key found... add to currentparams
                        currentParams[paramKey] = currentPathParts[partcount];
                    }
                    else {
                        //key not foutn..
                        throw new Error("Weird Route error..");
                    }
                }
                else {
                    console.log("----)))))");
                    console.log(part);
                    console.log(partcount);
                    console.log(currentPathParts);
                    if (part != currentPathParts[partcount]) {
                        isAMatch = false;
                        break;
                    }
                }
                partcount++;
            }
            if (isAMatch) {
                endpoint = __assign({}, routeEndpoint);
                break;
            }
        }
        console.log("Endpointafter looping...........");
        console.log(endpoint);
        if (endpoint) {
            endpoint.params = currentParams;
        }
        return endpoint;
    };
    Router.prototype.AddRoute = function (route) {
        this.Routes[route.id] = route;
    };
    return Router;
}());
export default Router;
//# sourceMappingURL=Router.js.map