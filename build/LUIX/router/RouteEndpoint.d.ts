import Elem from '../Elem';
export default interface RouteEndpoint {
    id: string;
    path: string;
    comp: Elem;
    params: {
        [key: string]: any;
    };
    pathParts: string[];
    hasParams: boolean;
    search: any;
}
