﻿ 
export default class AppState  {
  

    private  Refresh: (props: any)=>void;

    public  Initialize(refresh: (props: any)=>void){       
        this.Refresh = refresh;
    }

    private  state: any = {};

    public  getState(): any {
        return this.state;
    }

    public  getStateAt(key: string): any {
        return this.state[key];
    }

    public  postData(key: string, val: any, fullRefresh: boolean): any {
        this.state[key] = val;
        if( this.Refresh && fullRefresh){
            this.Refresh(this.state);
        }
        return this.state;
    }


    public  post(data: {[key: string]: any}, fullRefresh: boolean): any {
        this.state = {...this.state, ...data};
        if( this.Refresh && fullRefresh){
            this.Refresh(this.state);
        }
        return this.state;
    }
 
    public  removeData(key: string, fullRefresh: boolean): any {
        delete this.state[key];

        if( this.Refresh && fullRefresh){
            this.Refresh(this.state);
        }
        return this.state;
    }
}
 