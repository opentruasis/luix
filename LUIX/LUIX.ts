import AppState from './AppState';
import Elem from './Elem';
import Router from './router/Router';
import RouteEndpoint from './router/RouteEndpoint';

export class LUIX{


    private static Router: Router = new Router();

    private static _virtualDOM: Elem = null;

    public static  get VDOM(): Elem {
       return LUIX._virtualDOM;
    }

    public static set VDOM(vdom: Elem) {
        LUIX._virtualDOM = vdom;
    }

    private static initialize(){
        LUIX.VDOM = LUIX.buildDOMFromWindow(window);
    }

    private static buildDOMFromWindow(window: Window) : Elem{
        return new Elem("document", window.document);
    }

    private static _appState: AppState = new  AppState();

    static start(startElem: Elem){
        LUIX.initialize();
 
        LUIX._appState.Initialize(startElem.changed.bind(startElem));
 
        startElem.id = "literaluiapp";
        startElem.style = { "height": "100%" };
        let listOfNodes = LUIX.depthFirstSearch('literalui', LUIX.VDOM, []);
        for (let nd of listOfNodes) {
            nd.style = { "height": "100%" };
            nd.children.push(startElem);
            let literaluiElement = document.getElementById('literalui');
            literaluiElement.style.height = "100%";
            literaluiElement.appendChild(nd.render());
        }

        LUIX.post({"screenWidth":window.innerWidth, "screenHeight": window.innerHeight});

        window.addEventListener('resize', (event: any) => {
            console.log(event.target.innerWidth,  event.target.innerHeight);
            LUIX.post({"screenWidth": event.target.innerWidth, "screenHeight": event.target.innerHeight}, true);
        }, true);
 
    }

 
    private static depthFirstSearch(searchId: string, node: Elem, findings: Elem[]): Elem[]{
        if(node.id == searchId){
            findings.push(node);
        }
        for(let nd of node.children){
            this.depthFirstSearch(searchId, nd, findings);
        }
        return findings;
    }


    private static findParent(searchId: string, node: Elem): Elem {
        let result = null;
        for(var nd of node.children){
            if(nd.id == searchId){
                return node;
            }
        }
        for(var nd of node.children){
           result = LUIX.findParent(searchId, nd);
           if(result){
               return result;
           }
        }
        return result;
    }


    private static findElem(searchId: string, node: Elem): Elem{
        if(node == null){
            node = LUIX._virtualDOM;
            if(!node) return;
        }
        if(node.id == searchId){
            return node;
        }
        let result: Elem = null;
        for(var nd of node.children){
           result = LUIX.findElem(searchId, nd);
           if(result){
               return result;
           }
        }
        return result;
    }


    public static UpdateElem(updateNode: Elem){
        if(updateNode){
            var renderedData = updateNode.render();
            updateNode.replaceWith(renderedData);
            //focus  the focused element..
            updateNode.renderDone();
        }
    }
 

    public FocusedElement: Elem = null;
    //////////////////////////////////////////////////////////////////////////
    /////Router Interfaces////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////

    public static GoTo(path: string ):void {
       LUIX.Router.GoTo(path);
    }


    public static AddRoute(route: RouteEndpoint): void{
        LUIX.Router.AddRoute(route);
    }

    public static ProcessRoute(path: string): RouteEndpoint {
        return LUIX.Router.ProcessPath(path);
    }
    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////


    /////APP STATE Interface//////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////


    public static getState(): any {
        return  this._appState.getState();
    }

    public static  getStateAt(key: string): any {
        return this._appState.getStateAt(key);
    }

    public static postData(key: string, val: any, fullRefresh: boolean = true): any {
        return this._appState.postData(key, val, fullRefresh); 
    }


    public static post(data: {[key: string]: any}, fullRefresh: boolean = true): any {
        return this._appState.post(data, fullRefresh)
    }
 
    public static  removeData(key: string, fullRefresh: boolean = true): void {
        return this._appState.removeData(key, fullRefresh);
    }

    //////////////////////////////////////////////////////////////////////////
}


export default LUIX;