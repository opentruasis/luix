import LUIX  from './LUIX';

import { v4 as uuidv4 } from 'uuid';

import RouteEndpoint from './router/RouteEndpoint';
 
export class Elem{

    public type: string;
    private _id: string;
    public get id(): string {
        return this._id
    }
    public set id(iD: string) {
        this._id = iD;
    }

    public attrs: any;
    public children: Elem[]

    private elem: HTMLElement;
    
    public get Elem(): HTMLElement {
      return this.elem;
    }

    public props: any;

    constructor(type : string, props: any){
      if (type == null || typeof type !== 'string') {
        throw Error('The element type must be a string');
      }
      
      

      this.elem = document.createElement(type);
 
      const { id = null, children = [], style = {} } = props || {};
 
      Object.assign(this.elem.style, style);
      this.props = props;
      this.id = id || uuidv4();
     // this.elem.id = this.id;
      this.cleanAndSet(type, children);
    }

    private cleanAndSet(type : string, children: any = []):void{
     
      let cleanedChildren : Elem[] = [];
      if(children.length > 0){
        for(var k of children){
          cleanedChildren.push( this.cleanChildren(k) );
        }
      }
      this.type = type;
      this.children = cleanedChildren;
    }

 
    public changed(props: any): void{

       if(Object.keys(this.Routes).length > 0){
         //Do Routing instead of updating..
        
         var currentRoute = LUIX.getStateAt("currentRoute");
         if(currentRoute){
          var currentPath = currentRoute.path;
          
          if(currentPath != this.LastURL){
   
            if(currentRoute.id in this.Routes){
                
                this.clear();
                this.children.push(currentRoute.comp);
            }

          }

          this.LastURL = currentPath;
          }
       } 
      
       LUIX.UpdateElem(this);
 
    
        this.children.forEach(ch=>{
          if(ch){
            ch.changed(props);
          }
        });
 
   
    }

    private cleanChildren(element: any): Elem{
      return new Elem(element.nodeName, element);
    }
 
    public render():HTMLElement{
        //has A child with focused Element.
        this.elem.append(...this.children.map(ch=>ch.render()));
        return this.elem;
    }

    public renderDone(){
      if(this.isFocused){
     
        console.log("Render Done..")
        console.log(this.isFocused);
        this.elem.focus();
      //  (this.elem as HTMLInputElement).select();
     //   (this.elem as HTMLInputElement).setSelectionRange(-1,-1);
      }
    }

    public isFocused: boolean = false;

    public set onClick(event: ()=>void){
      this.elem.onclick = event;
    }

    public set onChange(event: (ev: Event)=>any){
      this.elem.addEventListener("input", event);
    }

   
    public set innerText(content: string){
        this.elem.innerText = content;
    }

    public set innerHtml(content: string){
      this.elem.innerHTML = content;
    }

    public set disabled(isDisabled: boolean){
      (this.elem as HTMLButtonElement).disabled = isDisabled;
    }

    private styles: any = {};

    public set style(styles: any) {
        this.styles = { ...this.styles, ...styles };
       Object.assign(this.elem.style, styles);
    }

    public get style(): any {
        return this.styles;
    }

    public addClass(className: string) {
        this.elem.classList.add(className);
    }

    public removeClass(className: string) {
        this.elem.classList.remove(className);
    }

    public replaceWith(...nodes: (string | Node)[]): void{
      this.elem.replaceWith(...nodes);
    }

    public clear(){
      this.children.splice(0,this.children.length);
      this.elem.innerHTML = "";
     // LiteralUI.ClearChildren(this);
    }

    public get src(): string {
        (this.elem as HTMLImageElement).src 
        return (this.elem as HTMLImageElement).src;
    }

    public set src(val: string)  {
        (this.elem as HTMLImageElement).src = val;
    }

    public set SelectionChanged(event: (ev: Event)=>any){
      (this.elem as HTMLSelectElement).onchange = event;
    }

    public get SelectedIndex(): number{
      return (this.elem as HTMLSelectElement).selectedIndex;
    }

    public set SelectedIndex(val: number){
        (this.elem as HTMLSelectElement).selectedIndex = val;
    }

    public get Selected(): string{
      return (this.elem as HTMLSelectElement).value;
    }

    public set inputType(val: string)  {
      (this.elem as HTMLInputElement).type = val;
    }

   public get inputType(): string{
     return (this.elem as HTMLInputElement).type;
   }

   public get files(): any{
     return (this.elem as HTMLInputElement).files;
   }

   public focus(){
     (this.elem as HTMLInputElement).focus();
   }

   public set value(val: string)  {
    (this.elem as HTMLInputElement).value = val;
  }

  public get value(): string{
    return  (this.elem as HTMLInputElement).value;
  }
 

  public scrollEnd(){
    this.elem.scrollLeft += this.elem.scrollWidth;
  }

  public scrollLeft(delta:number){
    this.elem.scrollLeft += delta;
  }

  public set onWheel(event: (ev: WheelEvent)=>any){
    this.elem.onwheel = event;
  }

  public set onScroll(event: (ev:Event)=>any){
    this.elem.onscroll = event;
  }

  
  public isFlexDisplay: boolean = false;

  public set visible(val: boolean){
 
    if(val){
      if(this.isFlexDisplay){
        this.elem.style.display = "flex";
      }else{
        this.elem.style.display = "block";
      }
    }else {
      this.elem.style.display = "none";
    }
    
  }


  public get visible():boolean{
     return this.elem.style.display !== "none";
  }
  
  ///SubRouts:
  private LastURL: string = "";
  public Routes: {[key:string]:RouteEndpoint} = {};

 
  public AddRoute(path: string, comp: Elem): void {
    var route = {} as RouteEndpoint;

    route.id = uuidv4();
    var pathParts = path.split("/");
    pathParts.shift();
    var params : {[key:string]: any} = {};
 
    for(var part of  pathParts){ 
        if(part.indexOf("$", 0) == 0){
          params[part.substring(1)] = null;
        } 
    } 
 
    route.path  = path;
    route.comp = comp;
    route.pathParts = pathParts;
    route.params = params;
    if( Object.keys(params).length > 0){
      route.hasParams = true;
    }else{
      route.hasParams = false;
    }
 
    //route["params"] = string;
    this.Routes[route.id] = route;
    LUIX.AddRoute(route);
    
    
  }

}


export default Elem;
 
 