import AppState from "../AppState";
import Elem from '../Elem';
import LUIX from "../LUIX";
import RouteEndpoint from './RouteEndpoint';
 
export default class Router {

    constructor(){
 
        window.addEventListener('popstate', (event) => {
            let currentPath = window.history.state ? window.history.state : "/";
            
            
            var route =  this.ProcessPath(currentPath);
            var searchObj = this.ProcessSearch(window.location.search);
            if(route){
              route.search = searchObj;
              LUIX.postData("currentRoute", route);
            }
        }, false);

        setTimeout(()=>{
            let currentPath = window.location.pathname ? window.location.pathname : "/";
            var route = this.ProcessPath(currentPath);
            var searchObj = this.ProcessSearch(window.location.search);
            if(route){
              route.search = searchObj;
              LUIX.postData("currentRoute", route);
            }
        },10);
    }
 
 
    public GoTo(path: string ):void {
        window.history.pushState(path, path, path);
        var route = this.ProcessPath(path);
        var searchObj = this.ProcessSearch(path);
        route.search = searchObj;
        LUIX.postData("currentRoute", route);
    }

 
    private ProcessSearch(searchStr: string): any{
      try{
        var searchSplit = searchStr.split("?");
        searchSplit.shift();
        var searchKeyVal: {[key:string]:string} = {};
        if(searchSplit.length > 0){
         var multiQuery = searchSplit[0].split("&");
          for(var dataStr of multiQuery){
              var splitKeyVal = dataStr.split("=");
              searchKeyVal[splitKeyVal[0]] = splitKeyVal[1];
          }
        }
        return searchKeyVal;
      }catch(e){
        return {};
      }
    }



    public ProcessPath(path: string) : RouteEndpoint{
       console.log("Process Path...");
       console.log(path);
        let endpoint: RouteEndpoint = null;
    
        var currentPathParts = path.split("?");
        console.log("mmmmm");
        console.log(currentPathParts);

        if(currentPathParts.length == 2){
          currentPathParts = [currentPathParts[0]];
        }
        currentPathParts = currentPathParts[0].split("/");
        currentPathParts.shift(); // get rid of first elem.
        console.log(currentPathParts);
        var currentParams: {[key: string]: any} = {};
        for(var routeId in this.Routes){
            var routeEndpoint = this.Routes[routeId];
            console.log("......")
            console.log(routeEndpoint);
            let partcount = 0;
            let isAMatch = true;

            for(var part of routeEndpoint.pathParts){
 
              if(part.indexOf("$", 0) == 0){
                //is a variable position..
                //then param..
                var paramKey = part.substring(1);
 
                if(paramKey in routeEndpoint.params){
                    //key found... add to currentparams
                    currentParams[paramKey] = currentPathParts[partcount];
                }else{
                  //key not foutn..
                  throw new Error("Weird Route error..");
                }

              }else{
                console.log("----)))))");
                console.log(part);
                console.log(partcount);
                console.log(currentPathParts)
                if(part != currentPathParts[partcount]){
                  isAMatch = false;
                  break;
                }
              }

              partcount++;

            }

            if(isAMatch){
                endpoint = {...routeEndpoint};
                break;
            }
        }
        console.log("Endpointafter looping...........");
        console.log(endpoint);
         
        if(endpoint){
          endpoint.params = currentParams;
        }
        return endpoint;
    }
    
    private Routes: {[key:string]:RouteEndpoint} = {};
 
    public AddRoute(route: RouteEndpoint): void{
      this.Routes[route.id] = route;
    }
 
}
