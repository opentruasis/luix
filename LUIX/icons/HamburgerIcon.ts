import Elem from '../Elem';
 

export class HamburgerIcon extends Elem {
    constructor(props: any = null){
        super("DIV", props);
        var {width = 24, height = 24, color = "#333"} = props || {};
        this.style={width:width + 'px', height: height + 'px'};

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
         
        svg.setAttribute("width", width);
        svg.setAttribute("height", height);
        svg.setAttribute("viewBox", `0 0 24 24`);
        svg.setAttribute("fill", color);

        svg.innerHTML = `<path d="M0 0h24v24H0V0z" fill="none"/><path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"/>`;
        
        this.Elem.appendChild(svg);
    }
}

export default HamburgerIcon;
