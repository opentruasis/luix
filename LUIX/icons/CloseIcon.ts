import Elem from '../Elem';
 

export class CloseIcon extends Elem {
    constructor(props: any = null){
        super("DIV", props);
        var {width = 24, height = 24, color = "#333"} = props || {};
        this.style={width:width + 'px', height: height + 'px'};

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
         
        svg.setAttribute("width", width);
        svg.setAttribute("height", height);
        svg.setAttribute("viewBox", `0 0 24 24`);
        svg.setAttribute("fill", color);

        svg.innerHTML = `<path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"/>`;
        
        this.Elem.appendChild(svg);
    }
 
}     
export default CloseIcon;