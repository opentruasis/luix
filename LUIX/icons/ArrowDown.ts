import Elem from '../Elem';
 

export class ArrowDown extends Elem {
    constructor(props: any = null){
        super("DIV", props);
        var {width = 24, height = 24, color = "#333"} = props || {};
        this.style={width:width + 'px', height: height + 'px'};

        const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
         
        svg.setAttribute("width", width);
        svg.setAttribute("height", height);
        svg.setAttribute("viewBox", `0 0 24 24`);
        svg.setAttribute("fill", color);

        svg.innerHTML = `<path d="M0 0h24v24H0V0z" fill="none"/><path d="M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z"/>`;
        
        this.Elem.appendChild(svg);
    }
 
}     
export default ArrowDown;