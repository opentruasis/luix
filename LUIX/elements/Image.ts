import Elem from '../Elem';

export class Image extends Elem {
    constructor(props: any = null){
        super("IMG", props);
    }

    public get imgSource(): string {
        return this.src;
    }

    public set imgSource(value: string) {
         this.src = value;
    }

}

export default Image;