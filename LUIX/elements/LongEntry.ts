 
import {FlexBox} from './FlexBox';
import Textarea from './Textarea';
import Text from './Text';

export class LongEntry extends FlexBox{

    constructor(props: any = null){
        super(props);
        this.style={alignItems: 'flex-start', flexDirection: 'column', width: '100%'};
        this.EntryLabel.style={marginRight:"5px"};
        this.EntryText.style={marginRight:"5px"};
        this.push(this.EntryLabel);
        this.EntryText.style = {width: '90%', margin: '5px', alignSelf: 'center'};
        this.push(this.EntryText);
        
    }

    public EntryLabel: Text = new Text();

    public EntryText: Textarea = new Textarea();

 
    public render():HTMLElement{
       return super.render();
    }

    public changed(props: any){
        

        super.changed(props);
    }

}

export default LongEntry;