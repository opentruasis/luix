import Elem from '../Elem';

export class ProgressBar extends Elem {

    constructor(props: any = null){
        super("DIV", props);
        
        this.style={width: '100%', height: "20px", backgroundColor: "#333", border: "1px"};


    }
 
}
export default ProgressBar;