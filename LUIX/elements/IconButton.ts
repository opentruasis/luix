import Button from './Button';

export class IconButton extends Button{
    constructor(props: any = null){
        super(props);
        this.addClass("selectable");
        this.style = {fontSize: "20px", borderRadius: "20px", borderWidth: "2px", margin: "5px", overflow: 'hidden'};
        if (props != null) {
            this.onClick = props.onClick;
        }
    }
}


export default IconButton;