import Elem from '../Elem';
 
export class Canvas extends Elem {
 
    constructor(props: any = null){
        super("CANVAS", props);
    }
 
    
}     

export default Canvas;
