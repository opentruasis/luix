import {Elem} from '../Elem';
 
export class Input extends Elem{

    constructor(props: any = null){
        super("INPUT", props);
        this.inputType = "text";

        this.style={backgroundColor:"#ccc", borderWidth: "0px", borderRadius: "5px", minHeight: "25px"}
   
        this.Elem.onchange = ()=>{
            this.isFocused = true;
        }

        this.Elem.onfocus = ()=>{
            this.isFocused = true;
        }

        this.Elem.onblur = (e:any)=>{
            if(e.sourceCapabilities){
                this.isFocused = false;
            } 
        }
    }

 
 
    public get InputType(): string {
        return this.inputType;
    } 

    public set InputType(val: string) {
        this.inputType = val;
    } 


  
    public get Value(): string {
        return this.value;
    }

    public set Value(val:string){
        this.value = val;
    }

  
  
}


export default  Input;