import Elem from '../Elem';

export class Text extends Elem{

    constructor(txt:string = "", props: any = null){
        super("DIV", props);
        this.text = txt;
    }

    private text: string = "";
    public get Text(): string {
        return this.text;
    }

    public set Text(innerText: string) {
        this.text = innerText;
        this.innerText = innerText;
    }

    public render():HTMLElement{
       this.innerText = this.text;
       return super.render();
    }

    public changed(props: any){
        super.changed(props);
    }

}


export default Text;