import {FlexBox} from './FlexBox';
import Input from './Input';
import Label from './Label';
import Span from './Span';

export class Switch extends FlexBox {
    constructor(props: any = null){
        super(props);
 
        this.switchSpan.addClass("slider"); 
        this.switchSpan.addClass("round");
        this.switchLabel.addClass("switch");
        this.switchInput.InputType = "checkbox";
        
        this.switchLabel.children.push(this.switchInput);
 
        this.switchLabel.children.push(this.switchSpan);
        
        this.push(this.switchLabel);


        var elem = document.getElementById("SwitchCss");
      if(!elem){
        var styleElem = document.createElement('style');
        styleElem.id = "SwitchCss";
        styleElem.innerHTML = `
        .switch {
            position: relative;
            display: inline-block;
            width: 50px;
            height: 25px;
          }
          
          .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
          }
          
          .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
          }
          
          .slider:before {
            position: absolute;
            content: "";
            height: 25px;
            width: 25px;
         
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
          }
          
          input:checked + .slider {
            background-color: #333;
          }
          
          input:focus + .slider {
            box-shadow: 0 0 1px #333;
          }
          
          input:checked + .slider:before {
            -webkit-transform: translateX(25px);
            -ms-transform: translateX(25px);
            transform: translateX(25px);
          }
          
          /* Rounded sliders */
          .slider.round {
            border-radius: 25px;
            margin-left: 5px;
          }
          
          .slider.round:before {
            border-radius: 50%;
          }
         `;
        document.getElementsByTagName("head")[0].appendChild(styleElem);
      }
    }

    private switchLabel: Label = new Label();
    private switchInput: Input = new Input();
    private switchSpan: Span = new Span();

    public set LabelText(str: string){
      this.innerText = str;
      this.changed(this.props);
    }
  
    public set onSwitch(func: any){
        this.switchInput.onChange = func;
    }

    public set Disabled(isDisabled: boolean) {
      (this.switchInput.Elem as HTMLInputElement).disabled = isDisabled;
    }

    public get isOn():boolean{
        return (this.switchInput.Elem as HTMLInputElement).checked;
    }

    public set isOn(val: boolean){
      (this.switchInput.Elem as HTMLInputElement).checked = val;
    }
 
}


export default Switch;