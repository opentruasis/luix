﻿import ElemList from './ElemList';
 
export class FlexBox extends ElemList {

    constructor(props: any = null ) {
        super(props);
 
        let {style = {}} = props || {};
        this.isFlexDisplay = true;
        this.style = {display:'flex', flexDirection: "row", justifyContent: 'space-around',...style};
    }
    
}     
  
 export default FlexBox;

