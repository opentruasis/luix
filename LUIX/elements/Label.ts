import Elem from '../Elem';

export class Label extends Elem{

    constructor(txt:string = "", props: any = null){
        super("LABEL", props);
        this.text = txt;
    }

    public text: string = "";

    public render():HTMLElement{
       this.innerText = this.text;
       return super.render();
    }

    public changed(props: any){
        super.changed(props);
    }

}

export default Label;