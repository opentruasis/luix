import Elem from '../Elem';

export class Button extends Elem {

    constructor(props: any = null){
        super("BUTTON", props);
        this.innerText = "Click Me!!";
    }
 
}

export default Button;