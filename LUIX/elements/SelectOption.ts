import Elem from '../Elem';

export class SelectOption extends Elem {
    constructor(props: any = null){
        super("OPTION", props);
    }
 
}

export default SelectOption;