import Elem from '../Elem';
 
export class ElemList extends Elem {
 
    constructor(props: any = null){
        super("div", props);
    }
 
    public push(newElem: Elem) {
        this.children.push(newElem);
    }      

  
}     

export default ElemList;