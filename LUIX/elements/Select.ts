import Elem from '../Elem';
import SelectOption from './SelectOption';

export class Select extends Elem {

    constructor(props: any = null){
        super("SELECT", props);
    
        this.SelectionChanged = (event: Event) => {
            this.mySelectionIndex = this.SelectedIndex;
            this.onSelectedChanged();
            this.SelectedIndex = this.mySelectionIndex;
        };

    }
 
    public onSelectedChanged():void{
        alert("Override onSelectedChanged");
    };

    public mySelectionIndex: number = 0;
 
    private _optionList: SelectOption[] = [];

    public set OptionList(options: SelectOption[]){
        this.ClearOptions();
        this._optionList = options;
        this.children = this._optionList;
    }


    public override renderDone(){
       setTimeout(()=>{
            this.SelectedIndex = this.mySelectionIndex;
       },100);
    }
 

    public get OptionList(): SelectOption[]{
        return this._optionList;
    }

    public AddOption(option: SelectOption){
        this._optionList.push(option);
    }

    public RemoveOptionAt(key: number){
        this._optionList.splice(key,1);
    }

    public ClearOptions(){
        this._optionList = [];
        this._optionList.splice(0,this._optionList.length);
        this.clear();
    }

}

export default Select;