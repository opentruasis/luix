 
import {FlexBox} from './FlexBox';
import Input from './Input';
import Text from './Text';

export class Entry extends FlexBox {

    constructor(props: any = null){
        super(props);
        this.style={alignItems: 'center'}
        this.EntryLabel.style={marginRight:"5px"};
        this.EntryInput.style={marginRight:"5px"};
        this.push(this.EntryLabel);
        this.push(this.EntryInput);
        
    }

    public EntryLabel: Text = new Text();

    public EntryInput: Input = new Input();

 
    public render():HTMLElement{
       return super.render();
    }

    public changed(props: any){
        super.changed(props);
    }

}

export default Entry;