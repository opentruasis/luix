import Elem from '../Elem';
 
export class Textarea extends Elem{

    constructor(props: any = null){
        super("textarea", props);
 
        this.style={backgroundColor:"#ccc", borderWidth: "0px", borderRadius: "5px", minHeight: "25px"}
        this.Elem.onchange = ()=>{
            this.isFocused = true;
        }

        this.Elem.onfocus = ()=>{
            this.isFocused = true;
        }

        this.Elem.onblur = (e:any)=>{
            if(e.sourceCapabilities){
                this.isFocused = false;
            } 
        }
    }
 
}

export default Textarea;